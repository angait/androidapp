package edu.pitt.angait;

public class SamplingLifecycleUpdatesConstants {

	public final static int STARTING = 0;
	public final static int STARTED = 1;
	public final static int STOPPING = 2;
	public final static int STOPPED = 3;
	public final static int STATUS_UPDATE = 4;
	
	
	// Forbid instantiation
	private SamplingLifecycleUpdatesConstants() {}
}
