package edu.pitt.angait;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STARTED;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STARTING;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STATUS_UPDATE;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STOPPED;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STOPPING;

public class SamplingLifecycleUpdatesHandler extends Handler {

	private SamplingMonitorActivity activity;
	public SamplingLifecycleUpdatesHandler (SamplingMonitorActivity activity) {
		this.activity = activity;
	}
	
	@Override
	public void handleMessage(Message message) {
		switch (message.what) {
		case STARTING:
			activity.onSamplingStarting();
			break;
		case STARTED:
			activity.onSamplingStarted();
			break;
		case STOPPING:
			activity.onSamplingStopping();
			break;
		case STOPPED:
			activity.onSamplingStop();
			break;
		case STATUS_UPDATE:
			activity.onSamplingStatusUpdate(message.arg1);
			break;
		default:
			Log.d(getClass().getName(), "unknown lifecycle code " + 
					message.what);
			break;
		}
		
	}
}