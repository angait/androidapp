package edu.pitt.angait;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BTConnector {
	private static final String TAG = "BTCLIENT";
    
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothSocket btSocket = null;
    private BluetoothDevice btDevice = null;
    private static final String SPP_UUID = 
    		"00001101-0000-1000-8000-00805F9B34FB";
    private static final UUID mUUID = 
    		UUID.fromString(SPP_UUID);
    
    BTConnector(BluetoothDevice btDevice) {
    		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    		this.btDevice = btDevice;
    }
    
    public boolean bluetoothAvailable() {
    	return mBluetoothAdapter != null;
    }
    
    public boolean bluetoothEnabled() {
    	return mBluetoothAdapter.isEnabled();
    }
    
    public BluetoothSocket connect(){
    	if(btSocket == null && btDevice != null) {
    	
    		try {
    			btSocket = btDevice.createRfcommSocketToServiceRecord(mUUID);
    		} catch (IOException e) {
    			Log.e(TAG, "Socket creation failed.", e);
    			return btSocket;
    		}
    	}
   
    	//mBluetoothAdapter.cancelDiscovery();
    	
    	try {
            btSocket.connect();
            Log.d(TAG, "BT connection established, data transfer link open.");
    	} catch (IOException e) {
    		Log.e(getClass().getName().toString(), e.toString());
    		try {
    			Log.d(getClass().getName().toString(), "Attempting a insecure connection for buggy devices");
    			btSocket = 
    				btDevice.createInsecureRfcommSocketToServiceRecord(mUUID);
    			btSocket.connect();
    		} catch (IOException eFailsafe) {
    			Log.d(getClass().getName().toString(), eFailsafe.toString());
    			try {
       			 	btSocket.close();
       			 	btSocket = null;
    			} catch (IOException e2) {
    				Log.e(TAG, "Unable to close socket during connection failure");
    				btSocket = null;
    			}
    			try {
    				Method m = btDevice.getClass()
    						.getMethod("createRfcommSocket", new Class[] {int.class});
					btSocket = (BluetoothSocket) m.invoke(btDevice, 1);
    			} catch (InvocationTargetException apiEx) {
    				Log.d(getClass().getName().toString(), apiEx.toString());
        			try {
           			 	btSocket.close();
           			 	btSocket = null;
        			} catch (IOException e2) {
        				Log.e(TAG, "Unable to close socket during connection failure");
        				btSocket = null;
        			}
    			} catch (NoSuchMethodException apiEx2) {
    				Log.d(getClass().getName().toString(), apiEx2.toString());
        			try {
           			 	btSocket.close();
           			 	btSocket = null;
        			} catch (IOException e2) {
        				Log.e(TAG, "Unable to close socket during connection failure");
        				btSocket = null;
        			}
    			} catch (IllegalAccessException apiEx3) {
    				Log.d(getClass().getName().toString(), apiEx3.toString());
        			try {
           			 	btSocket.close();
           			 	btSocket = null;
        			} catch (IOException e2) {
        				Log.e(TAG, "Unable to close socket during connection failure");
        				btSocket = null;
        			}
    			}
    			

    		}
    	}
    	
    	return btSocket;
    }
    
    public void close() {
    	try{
    		btSocket.close();
    	} catch(IOException e) {
    		Log.e(TAG, "Unable to close socket");
    	}
    }
                       
}
