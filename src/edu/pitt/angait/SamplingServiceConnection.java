package edu.pitt.angait;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;

public class SamplingServiceConnection implements ServiceConnection {
	private Messenger samplingServiceMessenger = null;
	
	private SamplingMonitorActivity activity;
	public SamplingServiceConnection(SamplingMonitorActivity activity) {
		this.activity = activity;
	}
	@Override
	public void onServiceConnected(ComponentName className,
			IBinder service) {
		samplingServiceMessenger = new Messenger(service);
		activity.onServiceBinding(samplingServiceMessenger);
		
	}

	@Override
	public void onServiceDisconnected(ComponentName className) {
		samplingServiceMessenger = null;
	}
	
	public Messenger getMessenger() {
		return this.samplingServiceMessenger;
	}
}