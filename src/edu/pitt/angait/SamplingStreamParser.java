package edu.pitt.angait;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.os.Environment;
import android.util.Log;

/**
 * Writes the content of a stream to an .agt file.
 * 
 */
public class SamplingStreamParser {
	
	static final String TAG = "SamplingStreamParser"; 
	private OutputStream out;
	private byte[] internalBuffer = new byte[16384]; // Max buffer length specified by android protocol
	
	static int readBytes = 0;
	static int availableBytes = 0;
	
	/**
	 * Creates a SamplingStreamParser that writes data to an .agt file
	 * in the AnGait_Data directory.
	 * 
	 * The file name is a timestamp in milliseconds corresponding to the
	 * time when the file was generated.
	 * 
	 * @throws IOException
	 */
	public SamplingStreamParser() throws IOException {
		//Note: filePath was changed to include the directory AnGait_Data
		String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()
				+ File.separator + "AnGait_Data" + File.separator + System.currentTimeMillis() + ".agt";
		File file = new File(filePath);
		file.createNewFile();
		if(file.exists()) {
			out = new BufferedOutputStream(new FileOutputStream(file, true));
		}
	}	
	
	/**
	 *  Reads an input stream and optionally returns a string.
	 *  @param inStream input stream to read
	 */
	public String[] parse(InputStream inStream) throws IOException {
		/* See DemoKitActivity.java in the android ADK for an exmaple
		 * of this try catch loop. Apparently, it's acceptable use from
		 * google. Hopefully, they'll fix their underlying accessory usb
		 * drivers to not screw this up.
		 */
		//availableBytes = inStream.available();
		//if(availableBytes > 0) {
		//}
		readBytes = inStream.read(internalBuffer);
		Log.v(TAG, readBytes + " bytes were read");
		if(readBytes>0) {
			out.write(internalBuffer,0,readBytes);
		}
		return null;
	}
	
	/**
	 * Flushes and closes the parser. 
	 */
	void close() throws IOException {
		out.flush();
		out.close();
	}
}