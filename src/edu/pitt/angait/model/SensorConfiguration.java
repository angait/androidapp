package edu.pitt.angait.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

public class SensorConfiguration implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
	private List<Sensor> samplingOrder;
    transient private BluetoothDevice device = null;
	private String deviceAddress;
    
    public SensorConfiguration(BluetoothDevice device, 
    		List<Sensor> samplingOrder) {
    	this.samplingOrder = samplingOrder;
        this.device = device;
        syncDeviceWithFields();
    }
    
    public List<Sensor> getSamplingOrder() {
    	return samplingOrder;
    }
    
    public BluetoothDevice getBluetoothDevice() {
    	return device;
    }
    
    private void syncDeviceWithFields() {
    	if(device == null) {
    		Set<BluetoothDevice> devices = 
    				BluetoothAdapter.getDefaultAdapter().getBondedDevices();
            for(BluetoothDevice potentialDevice : devices) {
            	if (potentialDevice.getAddress() == deviceAddress) {
            		device = potentialDevice;
            		break;
            	}
            }
    	}
    }
    private void writeObject(ObjectOutputStream out) 
    		throws IOException {
    	out.defaultWriteObject();
    }
    
    private void readObject(ObjectInputStream in) 
    		throws IOException, ClassNotFoundException {
    	in.defaultReadObject();
        syncDeviceWithFields();
    }

}
