package edu.pitt.angait.model;

import java.io.Serializable;

public class Sensor implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
	private int numBits = 0;
    private String sensorType = "";

	public Sensor(String sensorType, int numBits) {
		this.numBits = numBits;
        this.sensorType = sensorType;
	}
    public int getNumBits() {
    	return numBits;
    }
    public String getSensorType() {
    	return sensorType;
    }
}
