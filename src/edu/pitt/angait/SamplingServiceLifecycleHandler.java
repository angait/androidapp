package edu.pitt.angait;

import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STARTED;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STARTING;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STATUS_UPDATE;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STOPPED;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STOPPING;
import static edu.pitt.angait.SamplingService.REGISTER_LIFECYCLE_OBSERVER;
import static edu.pitt.angait.SamplingService.SEND_UPDATE;
import static edu.pitt.angait.SamplingService.START_SAMPLING;
import static edu.pitt.angait.SamplingService.STOP_SAMPLING;
import static edu.pitt.angait.SamplingService.UNREGISTER_LIFECYCLE_OBSERVER;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;

// Sole purpose is to manage requests to change or start a sampling thread
// will run based on two mechanisms... a continuous run when the time to 
// run is unspecified and a timed run
class SamplingThreadLifecycleHandler extends Handler {
	UsbManager usbManager;
	SamplingService samplingService; 
	public SamplingThreadLifecycleHandler(SamplingService context)
	{
		usbManager = 
				(UsbManager) context.getSystemService(Context.USB_SERVICE);
		this.samplingService = context;
	}
	private class SamplingThread extends Thread {

		// This is a timer that should pass a call to shutdown at an
		// appropriate moment

		// Note... this should only be used by a single calling thread
		// multithread invocation is unsafe

		private boolean tryToSample = true;
		private final static String TAG = "SamplingThread";
		private SamplingThreadLifecycleHandler handler;
		private ParcelFileDescriptor parcelFileDescriptor;

		public SamplingThread(
				ParcelFileDescriptor parcelFileDescriptor,
				SamplingThreadLifecycleHandler handler) 
		{
			this.handler = handler;
			this.parcelFileDescriptor = parcelFileDescriptor;	
		}

		@Override
		public void run() {
			// If a runtime is set, schedule a task that will stop the sampling at a given time.
			PowerManager pm = (PowerManager) samplingService.getSystemService(Context.POWER_SERVICE);
			PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ANGAIT_SAMPLING_WL");
			Log.i("SamplingThread", "running");
			InputStream inStream = null;
			OutputStream outStream = null;
			SamplingStreamParser parser = null;
			wl.acquire();
			try{
				parser = new SamplingStreamParser();
				/*
				inStream = new ParcelFileDescriptor.AutoCloseInputStream(parcelFileDescriptor);
				outStream = new ParcelFileDescriptor.AutoCloseOutputStream(parcelFileDescriptor);
				*/
				FileDescriptor fd = parcelFileDescriptor.getFileDescriptor();
				inStream = new FileInputStream(fd);
				outStream = new FileOutputStream(fd);
				
				if(outStream != null && inStream != null) 
				{
					long startTime = System.nanoTime();
					outStream.write('x');
					
					while(tryToSample) {
						parser.parse(inStream);
					}
					Long elapsedTime = Long.valueOf((System.nanoTime() - startTime)/1000000);					
					Log.i(TAG, "Elapsed time: " + elapsedTime.toString() + "ms.");
					outStream.write('s');

					outStream.close();
					inStream.close();
					
					
					
				}
				parcelFileDescriptor.close();
				parser.close();
			} catch(IOException e) {
				Log.e(TAG,"I/O error!");
				Log.e(TAG, e.toString());

				if (inStream != null) {
					Log.i(TAG, "Input stream not null: closing it.");
					try {
						inStream.close();
					} catch (IOException e2) {
						Log.e(TAG, "Could not close input stream.");
						Log.e(getClass().getName().toString(), 
								e2.toString());
					}
				}
				if (outStream != null) {
					Log.i(TAG, "Output stream not null: closing it.");
					try {
						outStream.close();
					} catch (IOException e2) {
						Log.e(TAG, "Could not close output stream.");
						Log.e(getClass().getName().toString(), 
								e2.toString());
					}
				}
				if (parser != null) {
					Log.i(TAG, "Parser not null: closing it.");
					try {
						parser.close();
					} catch (IOException e2) {
						Log.e(TAG, "Could not close parser.");
						Log.e(getClass().getName().toString(), 
								e2.toString());
					}
				}
				if (parcelFileDescriptor != null)
				{
					Log.i(TAG, "File descriptor not null: closing it.");
					try
					{
						parcelFileDescriptor.close();
					}
					catch (IOException e2)
					{
						Log.e(TAG, "Could not close file descriptor.");
						Log.e(getClass().getName().toString(), 
								e2.toString());
					}
				}
			}
			
			wl.release();
			
			if(tryToSample) {
				// Play a sound for the stop of sampling
				MediaPlayer mp = MediaPlayer.create(samplingService, R.raw.stop_alert);
	            mp.setOnCompletionListener(new OnCompletionListener() {
	                @Override
					public void onCompletion(MediaPlayer mp) {
	                    mp.release();
	                }
	
	            });
	            mp.start();
			}
			Message remoteStopMessage =  new Message();
			remoteStopMessage.what = SamplingService.STOP_SAMPLING;
			handler.sendMessage(remoteStopMessage);
			Log.i("SamplingThread", "stopped");
		}
		public void shutdown() {
			tryToSample = false;
		}
	}
	private class SamplingTimerTask extends TimerTask {
		private final String TAG = "SamplingTimerTask";
		SamplingThreadLifecycleHandler handler;

		public SamplingTimerTask(
				SamplingThreadLifecycleHandler handler) {
			this.handler = handler;
		}

		@Override
		public void run() {
			Log.i(TAG, "end sampling");
			Message remoteStopMessage =  new Message();
			remoteStopMessage.what = SamplingService.STOP_SAMPLING;
			handler.sendMessage(remoteStopMessage);
			samplingTimer.cancel();

		}
	}


	private int lifecycleStatus = STOPPED;
	private List<Messenger> lifecycleObserverMessengers = 
			new LinkedList<Messenger>();
	private SamplingThread samplingThread = null;
	Timer samplingTimer = null;


	/**
	 * Internally updates the status of sampling and notifies
	 * observers of such.
	 *  
	 * @param lifecycleStage an integer from {@link edu.pitt.angait.SamplingLifecycleUpdatesConstants}
	 * 						  that specifies the stage of sampling.
	 */
	private final void lifecycleChange(int lifecycleStage) {
		lifecycleStatus = lifecycleStage;
		lifecycleMessage(lifecycleStatus);
	}

	/** 
	 * Convenience method for 
	 * {@link #lifecycleMessage(Message) lifecycleMessage(Message)}.
	 * Creates {@link Message} with the <i>what</i> member set to the life cycle
	 * stage.
	 * 
	 * @param lifecycleStage an integer from {@link edu.pitt.angait.SamplingLifecycleUpdatesConstants}
	 * 						  that specifies the signal to send to observers.
	 */
	private final void lifecycleMessage(int lifecycleStage) {
		Message message = Message.obtain(null, lifecycleStage, 0, 0);
		this.lifecycleMessage(message);
	}

	/**
	 * Sends message to registered life cycle observers
	 * 
	 * @param lifecycleStage an integer from {@link edu.pitt.angait.SamplingLifecycleUpdatesConstants}
	 * 						  that specifies the signal to send to observers.
	 */
	private final void lifecycleMessage(Message message) {
		for(Messenger messenger : lifecycleObserverMessengers) {
			try {
				if (messenger != null) 
					// Note... you cannot reuse the same message for a 
					// given messenger. e.g. Message.obtain
					messenger.send(Message.obtain(message));
			} catch (RemoteException e) {
				Log.e("SamplingService (STARTING)",e.toString());
			}
		}
	}

	/**
	 * Convenience method for sending a messages internally
	 * 
	 * @param command an integer constant declared in SamplingService which
	 *                 describes the desired sampling stage to switch
	 */
	private void internalLifecycleSwitch(int command) {
		Message msg =  Message.obtain(null, command, 0, 0);
		sendMessage(msg);
	}
	
	ParcelFileDescriptor parcelFileDescriptor;
	
	static private final String TAG = "SamplingServiceLifecycleHandler";
	
	@Override
	public void handleMessage(Message msg) {
		switch(msg.what) {
		case START_SAMPLING:
			if(lifecycleStatus != STOPPED)
				return;

			lifecycleChange(STARTING);
			// unsafe cast, but a programmer error. Maybe add some
			// more safety (requires catching runtime exceptions though)
			UsbAccessory device = (UsbAccessory) msg.obj; 

			if (device == null) {
				internalLifecycleSwitch(STOP_SAMPLING);
				return;
			}
			// Setup the sampling thread
			parcelFileDescriptor = usbManager.openAccessory(device);

			if (parcelFileDescriptor == null) {
				internalLifecycleSwitch(STOP_SAMPLING);
				return;
			}

			samplingThread = 
					new SamplingThread(parcelFileDescriptor, this);
			
			int minutes = msg.arg1;

			if (minutes > 0) {
				samplingTimer = new Timer();
				samplingTimer.schedule(
						new SamplingTimerTask(this),
						minutes*60*1000);
			}

			samplingService.makeForeground();
			samplingThread.start();
			lifecycleChange(STARTED);
			
			
			// Clean up device
			device = null;
			break;

		case STOP_SAMPLING:
			lifecycleChange(STOPPING);
			if(samplingThread != null) {
				if (samplingThread.isAlive()) {
					// Stop timer if it was running
					samplingThread.shutdown();
				} else {
					Log.d(TAG,
							"samplingThread was not alive" +
							"on STOP_SAMPLING request");
				}
			} else {
				Log.d(this.getClass().getName(),
						"Duplicate STOP_SAMPLING detected. Ignoring.");
			}
			if (samplingTimer != null) {
				samplingTimer.cancel(); 
			}
			if (parcelFileDescriptor != null) {
				try {
					parcelFileDescriptor.close();
				} catch (IOException pFDEx) {
					Log.v(TAG, pFDEx.getMessage());
				}
			}
			samplingThread = null;
			samplingTimer = null;
			lifecycleChange(STOPPED);
			samplingService.yieldForeground();
			break;

		case SEND_UPDATE:
			// Create message reply and populate with proper status
			Message statusReply = Message.obtain(null, STATUS_UPDATE, 
					lifecycleStatus, 0);
			lifecycleMessage(statusReply);
			break;

		case REGISTER_LIFECYCLE_OBSERVER:
			lifecycleObserverMessengers.add(msg.replyTo);
			break;

		case UNREGISTER_LIFECYCLE_OBSERVER:
			lifecycleObserverMessengers.remove(msg.replyTo);
			break;

		default:
			Log.d(TAG, "Unhandled message of type " + msg.what);
			break;
		}
	}
}