package edu.pitt.angait;

import java.io.IOException;
import java.io.File;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;

import edu.pitt.angait.model.SensorConfiguration;
import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.util.Log;

public class IGaitReader {
	
	private static final String TAG = "IGAITREADER";
    private BluetoothSocket btSocket = null;
    
    private OutputStream outStream = null;
    private BufferedReader read = null;
    private InputStream inStream = null;
    
    private String FILENAME = System.currentTimeMillis() + "iGait_log.txt";
	private File f = null;
    private BufferedWriter bw = null;
    private char [] buffer = null;
    private StringBuilder sb = null;
    private int index = 0;
    public IGaitReader(BluetoothSocket bts, SensorConfiguration configuration) 
        throws IOException
    {
    	btSocket = bts;
    	
		try {
			outStream = btSocket.getOutputStream();
    		
            inStream = btSocket.getInputStream();
			f = new File(Environment.getExternalStorageDirectory().toString(),  
					FILENAME);
			bw = new BufferedWriter(new FileWriter(f));
            read = new BufferedReader(new InputStreamReader(inStream, "UTF-8"));
			buffer = new char[50];
		} catch(IOException e) {
			Log.e(TAG, "Unable to open streams2");
			Log.e(TAG, e.toString());
            throw e;
		}
		sb = new StringBuilder();
    }
    
    public boolean ready() {
    	return buffer != null;
    }
    
    public boolean start() {
    	try {
    		outStream.write((byte) 1);
    		return true;
    	} catch(IOException e) {
    		Log.e(TAG, "Unable to start collecting data");
    		return false;
    	}		
    }
    
    public void readData() throws IOException {
    	if (read.ready())
		{
    		String s = read.readLine();
            this.index++;
			sb.append(this.index + ", " + s.split(":")[1]);
			sb.append(", " + read.readLine().split(":")[1]);
			sb.append(", " + read.readLine().split(":")[1]);
			sb.append(", " + read.readLine().split(":")[1]);
			sb.append(", " + read.readLine().split(":")[1] + "\n");
		}
    }
    
    public void write() throws IOException {
    	if (sb.length() > 0)
    		bw.write(sb.toString());
    }
    
    public void close() {
    	try {
    		outStream.write((byte) 1);
    		outStream.close();
    		inStream.close();
    		read.close();
    		bw.close();
    	} catch(IOException e){
    		Log.e(TAG, "Unable to close streams");
    	}
    	
    }
}
