package edu.pitt.angait;

import java.io.File;
import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.os.Messenger;
import edu.pitt.angait.SamplingThreadLifecycleHandler;

/* SamplingService provides access for lifecycle control of the sampling process
 * with Bluetooth. This includes broadcasts of the state changes. It is bound
 * to increase its ranking?
 */

public class SamplingService extends Service {

    // Manage the starting and stopping of the Bluetooth sampling thread
    // and respond to inquiries about its state.

    public static final int STOP_SAMPLING = 0;
    public static final int START_SAMPLING = 1;
    public static final int REGISTER_LIFECYCLE_OBSERVER = 2;
    public static final int UNREGISTER_LIFECYCLE_OBSERVER = 3;
    public static final int SEND_UPDATE = 4;
    

    private Messenger serviceMessenger;
    @Override 
    public void onCreate() {
        SamplingThreadLifecycleHandler lifecycleHandler = 
        		new SamplingThreadLifecycleHandler(this);
        serviceMessenger =
                new Messenger(lifecycleHandler);
        /*This code checks to see if a directory "AnGait_Data" exists and creates it if not
         * Without the directory the StreamParser operation will fail because it points here.
         * This was added to provide cleaner access to storage. */
        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()
				+ File.separator + "AnGait_Data" + File.separator;
        File file = new File(filePath);
        if (!file.isDirectory()){
				file.mkdir();
        }
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // Return START_STICKY so that this always is present within the
        // application
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return serviceMessenger.getBinder();
    }
    @Override
	public void onDestroy() {
    	super.onDestroy();
    }
    
    public void makeForeground() {
    	
		Notification.Builder builder = (new Notification.Builder(this))
			.setSmallIcon(R.drawable.ic_stat_example)
			.setTicker("sampling in progress")
			.setWhen(System.currentTimeMillis())
			.setAutoCancel(false)
			.setContentTitle("sampling")
			.setContentText("sampling in progress");
		
		Notification n = builder.getNotification();
		
        startForeground(this.hashCode(), n);
    }
    
    public void yieldForeground() {
    	this.stopForeground(true);
    	//This Line updates the droid content library making the agt files available immediately
    	//after sampling i.e. when the sampling foreground ends
    	sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory() + File.separator + "AnGait_Data")));
    }
}