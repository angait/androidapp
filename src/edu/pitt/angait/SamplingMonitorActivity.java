package edu.pitt.angait;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.*;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.view.LayoutInflater;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STARTED;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STARTING;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STOPPED;
import static edu.pitt.angait.SamplingLifecycleUpdatesConstants.STOPPING;

/**
 * This is the main activity of the Android application.
 * 
 * @author iMED Lab
 * @see Activity
 */

public class SamplingMonitorActivity extends Activity {	
	/********************************* Attributes ********************************************/
	// String used in log messages
	private final String TAG = "angait-activity";
	
	// SamplingService related attributes
	SamplingService samplingService;

	private int samplingServiceState = STOPPED;
	private static final int ACTIVITY_RESULT_BT_ENABLE = 1;

	private SamplingServiceConnection samplingServiceConnection =
			new SamplingServiceConnection(this);
	
	private Messenger samplingLifecycleUpdatesMessenger = 
			new Messenger(new SamplingLifecycleUpdatesHandler(this));
	
	private Messenger samplingServiceMessenger = null;
	
	//Code to change filename
	private String fileName;
	private boolean fileToName = false;
	
	// USB related attributes
	private static final String ACTION_USB_PERMISSION = "edu.pitt.angait.USB_PERMISSION";
	private UsbAccessory selectedDevice = null;
	ArrayAdapter<AccessoryWrapper> spinnerArrayAdapter = null;
	PendingIntent mPermissionIntent;
	
	UsbManager mUsbManager = null;
	Boolean permissionGranted = false;
	
	/******************************* Methods ***********************************************/
	
	/**
	 * Finds the most recently modified file in a directory.
	 * @param dir the name of the directory.
	 * @return the most recently modified file.
	 */
	public static File lastFileModified(String dir) {
		File fl = new File(dir);
		File[] files = fl.listFiles(new FileFilter() {			
			@Override
			public boolean accept(File file) {
				return file.isFile();
			}
		});
		long lastMod = Long.MIN_VALUE;
		File choise = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choise = file;
				lastMod = file.lastModified();
			}
		}
		return choise;
	}
	
	/*
	 * USB Accessory broadcast reception tooling
	 */
	private final BroadcastReceiver mUsbReceiver =
			new BroadcastReceiver() {
		private UsbAccessory accessory = null;
	    @Override
		public void onReceive(Context context, Intent intent) {
	        // Get the intent's action
	    	String action = intent.getAction();
	        
	        // Get a reference to the accessory
	    	accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
	        
	    	// Permission to communicate with the accessory requested
	        if (ACTION_USB_PERMISSION.equals(action)) {
	        	if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) 
	        	{
	        		permissionGranted = true;
	        		
	        		if(accessory != null)
	        		{
	        			// Update the list of devices
	        			checkDevicesAndPopulate(); 
	        		}
	        	}
	        	else 
	        	{
	        		// Display a debug message in the log
	        		Log.d(TAG, "permission denied for accessory " + accessory);
	        		accessory = null;
	        	}
	        } 
	        // If an accessory was attached
	        else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) 
	        {
	        	// Update the list of devices
	        	checkDevicesAndPopulate();
	        } 
	        else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) 
	        {
	        	// Update the list of devices
	        	checkDevicesAndPopulate();
	        	
	        	// Stop sampling
	        	onSamplingStop();
	        }
	    }
	};
	
	/**
	 * Wraps an Accessory so that it can be displayed in a Spinner widget.
	 * @see Accessory 
	 */
	private class AccessoryWrapper {
		public UsbAccessory accessory;
		public AccessoryWrapper(UsbAccessory accessory) {
			this.accessory = accessory;
		}
		@Override
		public String toString() {
			return accessory.getModel();
		}
		
	};
	
	/**
	 * Listener for the Spinner widget that allows to select an accessory.
	 * This class is needed as the widget is created dynamically.
	 */
	private class DeviceSelectedListener implements OnItemSelectedListener
	{
		// If an item is selected
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) 
		{
			if(spinnerArrayAdapter != null)
				selectedDevice = spinnerArrayAdapter.getItem(position).accessory; // Get the corresponding accessory
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) 
		{
			selectedDevice = null;
		}
		
	};
	
	/**
	 * Updates the list of devices.
	 */
	private void checkDevicesAndPopulate() 
	{
		// Retrieve the list of accessories
		UsbAccessory[] accessoryList = mUsbManager.getAccessoryList();
		
		// Retrieve the Spinner widget
		Spinner deviceList = (Spinner) findViewById(R.id.device_list);
		
		spinnerArrayAdapter = 
				new ArrayAdapter<AccessoryWrapper>(this, 
						android.R.layout.simple_spinner_dropdown_item);

		int selectedDevicePosition = -1;
		
		// If accessories were detected
		if(accessoryList != null) 
		{
			// Update the content of the list
			List<AccessoryWrapper> spinnerList = new ArrayList<AccessoryWrapper>();
			for(UsbAccessory accessory : accessoryList)
			{
				spinnerList.add(new AccessoryWrapper(accessory));
			}
			spinnerArrayAdapter.addAll(spinnerList);
		}
		
		deviceList.setAdapter(spinnerArrayAdapter);
		
		if (selectedDevicePosition >= 0)
			deviceList.setSelection(selectedDevicePosition);
		
		deviceList.setOnItemSelectedListener(new DeviceSelectedListener());
	}
	
	/**
	 * Initializes the activity during its onCreate state.
	 * Starts the sampling service, register a USB broadcast receiver
	 * and asks the permission to communicate with a USB accessory.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Start the sampling service
		Intent serviceIntent = new Intent(this, SamplingService.class);
		startService(serviceIntent);
		
		super.onCreate(savedInstanceState);
		
		// Setup the main layout
		setContentView(R.layout.main);
		
		// Check if runtime has been saved and restore it if it is the case 
		if (savedInstanceState != null) 
		{	
			EditText runtimeLength = (EditText) findViewById(R.id.edit_runtime);
			String runtimeString = savedInstanceState.getString("runtime");
			
			if (runtimeString != null)
				runtimeLength.setText(runtimeString);
		}
		
		// Retrieve the USB manager
		mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
		
		// Request the permission to communicate with an accessory
		mPermissionIntent = 
				PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0); 
		mUsbManager.requestPermission(selectedDevice, mPermissionIntent);
		
		// Update the list of devices
		checkDevicesAndPopulate();
	}
	
	/**
	 * Saves the runtime information to a Bundle.
	 * @see Bundle
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		EditText runtimeLength = (EditText) findViewById(R.id.edit_runtime);
		
		outState.putString("runtime", runtimeLength.getText().toString());
	}
	
	/**
	 * Initializes the activity during its onStart state.
	 * Starts the sampling service. 
	 */
    @Override
    public void onStart() {
        super.onStart();
        
        LayoutInflater inflater = this.getLayoutInflater();
        try 
        {
	        View mainLayout = (View) inflater.inflate(R.layout.main, null);
			mainLayout.setVisibility(View.INVISIBLE);
        } 
        catch (InflateException e) {
        	// Log because this shouldn't happen in a compiled class
        	Log.e(this.getClass().getName(), "Unable to find layout!");
        }
		
        // Start the service (if it has not already been started)
		Intent serviceIntent = new Intent(this, SamplingService.class);
		this.startService(serviceIntent);
		
		// Bind to the service
		this.bindService(serviceIntent,
				         this.samplingServiceConnection,
				         Context.BIND_AUTO_CREATE);
		
		// Register the USB accessory broadcast receiver
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		registerReceiver(mUsbReceiver, filter);
		
		// Update the list of devices
		checkDevicesAndPopulate();
    }
    
    /**
     * Clean the activity during its onStop state.
     */
	@Override
	public void onStop() {
		super.onStop();
		
		/* Unhook activity instance from receiving requests */
		Message unregisterRequest = Message.obtain(null, 
				SamplingService.UNREGISTER_LIFECYCLE_OBSERVER, 0, 0);
		
		unregisterRequest.replyTo = samplingLifecycleUpdatesMessenger;
		
		/* Unregister intent receivers */
		unregisterReceiver(mUsbReceiver);
		
		Messenger serviceMessenger = this.samplingServiceConnection.getMessenger();
		if (serviceMessenger != null)
			try 
		    {
				serviceMessenger.send(unregisterRequest);
			} 
			catch (RemoteException e) 
			{
				Log.e(this.getClass().getName(), "Unable to contact service.");
			}
		
		else
			Log.e(this.getClass().getName(),
				"Unable to start messenger to service.");
		
		if (this.samplingServiceConnection.getMessenger() != null) 
		{
			unbindService(this.samplingServiceConnection);
		}
	}

	@Override
	public void onActivityResult(int request, int result, Intent data) {
		switch (request) {
		case ACTIVITY_RESULT_BT_ENABLE:
			if(result == Activity.RESULT_CANCELED) {
				// Display that this is necessary
				checkDevicesAndPopulate();
			}
			break;
		default:
			Log.d(getClass().getName() +".onActivityResult()",
					"Unknown activity result code.");
			break;
		}
	}
	
	
	
	/*This function renames the unfriendly milli second based files names to a name specified by the user.
	 * If the specified file name is already taken the function will add a (i) to the end of the name
	 * If no filename was specified the function will do nothing and inform the user no input was provided
	 * Note: this function will check to make sure a file has been written and will not allow a user to
	 * directly rename a file after it has been named. This was done to make andgait more fail safe*/
	/*
	public void newFileName(View view){
		//check if there is a file to name
		if(fileToName == true){
			//if there is a file, get the name specified by the user
			fileName = ((EditText)findViewById(R.id.filename)).getText().toString();
			// set file directory needed for lastFileModified() function
			String dir = Environment.getExternalStorageDirectory().getAbsolutePath()
					+ File.separator + "AnGait_Data" + File.separator;
			//Find most recently modified file
			File oldName = lastFileModified(dir);
			//Initiate the new file name and create the file
			String newFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()
					+ File.separator + "AnGait_Data" + File.separator + fileName + ".agt";
			File newName = new File(newFilePath);
			//Detect if filename has been entered and whether or not it is original, renames millisecond files, and tells the app the most recent file has been renamed
			if (fileName.trim().equals("")){
				CharSequence text = this.getString(R.string.no_file_name);
				Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
				}
			else if(newName.exists()){
				int i=1;
				boolean fileWritten = false;
				while (fileWritten == false){
					newFilePath = Environment.getExternalStorageDirectory().getAbsolutePath()
							+ File.separator + "AnGait_Data" + File.separator + fileName + "("+ i +")" + ".agt";
					newName = new File(newFilePath);
					if (!newName.exists()){
						oldName.renameTo(newName);
						fileWritten = true;
						String string = "File named " + fileName + "("+ i +")";
						Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
							}
					else {
						i++;
							}
						}
				fileToName = false;
				}
			else if (!newName.exists()){
				oldName.renameTo(newName);
				fileToName = false;
				String string = "File named " + fileName;
				Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
				}
			else{
				//This should never happen, but was included just in case
				CharSequence text = this.getString(R.string.derp_file_name);
				Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
				}
				
		}
		else{
		CharSequence text = this.getString(R.string.no_file);
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
		}
	}
	*/
	/**
	 * Sampling button callback method that starts or stops
	 * the sampling service. 
	 * 
	 */
	public void toggleSampling(View toggler) {

		if (null == samplingServiceMessenger) {
			Log.d(this.getClass().getName()+"toggleSampling",
					"Unable to get samplingServiceMessenger. Ignoring toggle");
			return;
		}
		
		Message msg = null;
        
		// Check the sampling service state
		switch (this.samplingServiceState) 
		{
			case STOPPED:
				// If no device are selected
				if (selectedDevice == null) 
				{
					// Notify the user
					CharSequence text = this.getString(R.string.no_device_selected);
					Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
					return;
				}
				// If a device is selected but we require a permission to communicate with it
				else if (!mUsbManager.hasPermission(selectedDevice)) {
					// Notify the user
					CharSequence text = this.getString(R.string.no_permission_granted);
					Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
					
					// Request permission
					mUsbManager.requestPermission(selectedDevice, mPermissionIntent);
					return;
				}
				// Fallthrough case
				
				// Create a message that will start the sampling
				msg = Message.obtain(null, SamplingService.START_SAMPLING, 0, 0);
	            
				// Retrieve the runtime entered by the user
				EditText runtimeWidget = ((EditText) 
	            		findViewById(R.id.edit_runtime));
	            
	            int runtime = 0;
	            try {
		            runtime = Integer.valueOf(runtimeWidget.getText().toString());
	            } 
	            catch (NumberFormatException e) 
	            {
	            	Log.d(this.getClass().getName(),
	            			"Number formatting problem assuming. " +
	            			"Ignoring runtime \n" + e.toString());
	            }
	            msg.arg1 = runtime;
	            msg.obj = selectedDevice;
	            
	            fileToName = true;
	            break;
			
			case STARTED:
				// Create a message that will stop the sampling
				msg = Message.obtain(null, SamplingService.STOP_SAMPLING, 0, 0);
				break;
				
			default:
				Log.d(this.getClass().getName()+".toggleSampling()",
				  "STARTING/STOPPING detected on sampling press.");
				
		} // end switch
		
		// Send a message to the service (either start or stop sampling)
		msg.replyTo = this.samplingLifecycleUpdatesMessenger;
		try 
		{
			samplingServiceMessenger.send(msg);
		} 
		catch (RemoteException e) {
			Log.e(this.getClass().getName(),e.toString());
		}
	}
	
	public void onSamplingStarting() {
        Button samplingButton = 
        		(Button) this.findViewById(R.id.sampling_control);
        EditText minutesEntry = 
        		(EditText) this.findViewById(R.id.edit_runtime);
        samplingButton.setText("Starting...");
        samplingButton.setEnabled(false);
        minutesEntry.setEnabled(false);
        this.samplingServiceState = STARTING;
	}
	
	public void onSamplingStarted() {
        View samplingInProgressView  = 
        		this.findViewById(R.id.sampling_in_progress);
        Button samplingButton = 
        		(Button) this.findViewById(R.id.sampling_control);
        EditText minutesEntry = 
        		(EditText) this.findViewById(R.id.edit_runtime);
        
        samplingInProgressView.setVisibility(View.VISIBLE);
        samplingButton.setText("Stop");
        minutesEntry.setEnabled(false);
        samplingButton.setEnabled(true);
        this.samplingServiceState = STARTED;
	}
	
	public void onSamplingStopping() {
        Button samplingButton = 
        		(Button) this.findViewById(R.id.sampling_control);
        samplingButton.setText("Stopping...");
        samplingButton.setEnabled(false);
        this.samplingServiceState = STOPPING;
	}
	
	public void onSamplingStop() {
        View samplingInProgressView  = 
        		this.findViewById(R.id.sampling_in_progress);
        Button samplingButton = 
        		(Button) this.findViewById(R.id.sampling_control);
        EditText minutesEntry = 
        		(EditText) this.findViewById(R.id.edit_runtime);
        samplingInProgressView.setVisibility(View.INVISIBLE);
        samplingButton.setText("Start");
        samplingButton.setEnabled(true);
        minutesEntry.setEnabled(true);
        
        this.samplingServiceState = STOPPED;
	}

	// Designed to restart the interface from a status update. This is usually 
	// requested on startup or restart of the activity.
	
	public void onSamplingStatusUpdate(int status) {

	    LayoutInflater inflater = this.getLayoutInflater();
	    try {
	        View mainLayout = inflater.inflate(R.layout.main, null);
			mainLayout.setVisibility(View.VISIBLE);
	    } catch (InflateException e) {
	    	// Log because this shouldn't happen in a compiled class
	    	Log.e(this.getClass().getName(), "Unable to find layout!\n" + 
	    			e.toString());
	    }
	    checkDevicesAndPopulate();
		switch (status) {
			case STARTING:
				onSamplingStarting();
				break;
			case STARTED:
				onSamplingStarted();
				break;
			case STOPPING:
				onSamplingStopping();
				break;
			case STOPPED:
				onSamplingStop();
				break;

		}
		
	}

	public void onServiceBinding(Messenger samplingServiceMessenger) {
		this.samplingServiceMessenger = samplingServiceMessenger;
		Message updateRequest = Message.obtain(null, 
				SamplingService.SEND_UPDATE, 0, 0);
		Message registrationRequest = Message.obtain(null, 
				SamplingService.REGISTER_LIFECYCLE_OBSERVER, 0, 0);
		registrationRequest.replyTo = samplingLifecycleUpdatesMessenger;
		Messenger serviceMessenger = 
				this.samplingServiceConnection.getMessenger();
		if (serviceMessenger != null)
			try {
				samplingServiceMessenger.send(registrationRequest);
				samplingServiceMessenger.send(updateRequest);
			} catch (RemoteException e) {
				Log.e(this.getClass().getName(),
					"Unable to contact service."); 
			}
		else
			Log.e(this.getClass().getName(),
				"Unable to start messenger to service."); 
	}
}